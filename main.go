package main

import (
	"encoding/json"
	"encoding/xml"
	"errors"
	"fmt"
	"image"
	"image/png"
	"io/ioutil"
	"os"
	"os/exec"
	"strconv"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"github.com/disintegration/imaging"
	"github.com/kbinani/screenshot"
)

func main() {

	var err error

	screenshot_file := "screenshot.png"
	optimised_file := "ocr_ready.png"
	ocr_file := "output"
	language := "eng"
	screen := 0
	magnification := 1.5
	min_confidence := 90

	test_item := "loki_prime_systems"
	user_status := "ingame"

	fmt.Printf("(Screen, Magnification):(%v,%v)->Screenshot: %s -> Optimized:%s + Language: %s+ Min. Confidence: %v->Output: %s.hocr\n", screen, magnification, screenshot_file, optimised_file, language, min_confidence, ocr_file)
	fmt.Printf("Item: %s + User Status: %s\n", test_item, user_status)

	//	err = make_screenshot(screen, magnification, screenshot_file)
	if err != nil {
		panic(err)
	}
	//optimize_Image_For_OCR(screenshot_file, optimised_file)
	if err != nil {
		panic(err)
	}

	//err = ocr(optimised_file, ocr_file, language)
	if err != nil {
		panic(err)
	}

	var doc OCR_Document
	doc, err = decode_OCR_File(ocr_file + ".hocr")
	if err != nil {
		panic(err)
	}
	shell_Output_OCR_Document(doc, min_confidence)

	//	price, err := find_WarframeMarket_Min_Sell(test_item, user_status)
	//	if err != nil {
	//		panic(err)
	//	}
	//	fmt.Println("Price: ", price)

}

func shell_Output_OCR_Document(doc OCR_Document, min_confidence int) {
	for _, carea := range doc.Page.Careas {
		var strcarea string
		for _, par := range carea.Pars {
			var strpar string
			for _, line := range par.Lines {
				var strline string
				for _, word := range line.Words {
					var conf int
					conf, _ = word.Confidence()
					if conf >= min_confidence {
						strline += word.Content() + " "
					}
				}
				strline = strings.Trim(strline, " ")
				if len(strline) > 0 {
					strpar += strline + "\n"
				}
			}
			if len(strpar) > 0 {
				strcarea += strpar
			}
		}
		fmt.Print(strcarea)
	}
	fmt.Print("\n")
}

func optimize_Image_For_OCR(filename_original string, filename_target string) error {
	out, err := imaging.Open(filename_original)
	if err != nil {
		return err
	}

	out = imaging.AdjustGamma(out, 0.5)
	out = imaging.Sharpen(out, 100)
	out = imaging.AdjustContrast(out, 100)
	out = imaging.Grayscale(out)
	out = imaging.Invert(out)

	return imaging.Save(out, filename_target)
}

func decode_OCR_File(filename string) (OCR_Document, error) {
	var err error
	var content []byte
	xhtml := OCR_Document{}

	content, err = ioutil.ReadFile(filename)
	if err != nil {
		return xhtml, err
	}

	err = xml.Unmarshal(content, &xhtml)
	return xhtml, err
}

//OCR hiracy:
//Document>Page>[]Careas>[]Pars>[]Lines>[]Words
type OCR_Document struct {
	Page OCR_Page `xml:"body>div"`
}
type OCR_Page struct {
	Class  string      `xml:"class,attr"`
	Id     string      `xml:"id,attr"`
	Title  string      `xml:"title,attr"`
	Careas []OCR_Carea `xml:"div"`
}

type OCR_Carea struct {
	Class string    `xml:"class,attr"`
	Id    string    `xml:"id,attr"`
	Title string    `xml:"title,attr"`
	Pars  []OCR_Par `xml:"p"`
}

type OCR_Par struct {
	Class    string     `xml:"class,attr"`
	Id       string     `xml:"id,attr"`
	Title    string     `xml:"title,attr"`
	Language string     `xml:"lang"`
	Lines    []OCR_Line `xml:"span"`
}

type OCR_Line struct {
	Class string     `xml:"class,attr"`
	Id    string     `xml:"id,attr"`
	Title string     `xml:"title,attr"`
	Words []OCR_Word `xml:"span"`
}

type OCR_Word struct {
	Class  string `xml:"class,attr"`
	Id     string `xml:"id,attr"`
	Title  string `xml:"title,attr"`
	Value  string `xml:",chardata"`
	Em     string `xml:"em"`
	Strong string `xml:"strong"`
}

//Returns the Confidence of the OCR regarding the word.
//The Value is between 0 and 100
func (word OCR_Word) Confidence() (int, error) {
	if !strings.Contains(word.Title, ";") {
		return 0, errors.New("The format of the ocr word is diffrent, than expected!(No ';' found)")
	}

	box_part := strings.Split(word.Title, ";")[1]
	cords := strings.Split(box_part, " ")
	if len(cords) != 3 {
		return 0, errors.New("The format of the ocr word is diffrent, than expected!(No confidence found)")
	}

	return strconv.Atoi(cords[2])
}

//Returns the Box Coordinates, in which the OCR Word is positioned
func (word OCR_Word) Box() (image.Rectangle, error) {
	if !strings.Contains(word.Title, ";") {
		return image.Rectangle{}, errors.New("The format of the ocr word is diffrent, than expected!(No ';' found)")
	}

	box_part := strings.Split(word.Title, ";")[0]
	cords := strings.Split(box_part, " ")
	if len(box_part) != 5 {
		return image.Rectangle{}, errors.New("The format of the ocr word is diffrent, than expected!(No ")
	}

	var x0, y0, x1, y1 int
	var err error
	x0, err = strconv.Atoi(cords[1])
	if err != nil {
		return image.Rectangle{}, err
	}
	y0, err = strconv.Atoi(cords[1])
	if err != nil {
		return image.Rectangle{}, err
	}
	x1, err = strconv.Atoi(cords[1])
	if err != nil {
		return image.Rectangle{}, err
	}
	y1, err = strconv.Atoi(cords[1])
	if err != nil {
		return image.Rectangle{}, err
	}

	return image.Rect(x0, y0, x1, y1), nil
}

func (word OCR_Word) Content() string {
	ret := word.Value
	if ret == "" {
		ret = word.Em
	}
	if ret == "" {
		ret = word.Strong
	}
	return ret
}

//Returns the Minimum Sell Order for which you can buy the item on Warframe Market
//
//possible user_status are: offline,online, ingame
func find_WarframeMarket_Min_Sell(item string, user_status string) (int, error) {
	var json_item string
	var err error

	json_item, err = extract_WarframeMarket_Item(item)
	if err != nil {
		return 0, err
	}

	var item_orders []Order
	item_orders, err = decode_WarframeMarket_Json(json_item)
	if err != nil {
		return 0, err
	}
	return find_Min_Sell_Order(item_orders, "ingame"), nil
}

func decode_WarframeMarket_Json(json_str string) (orders []Order, err error) {
	var market WarframeMarket
	if err = json.Unmarshal([]byte(json_str), &market); err != nil {
		return
	}
	orders = market.Payload.Orders
	return
}

type WarframeMarket struct {
	Current_user interface{}
	Payload      struct {
		Orders []Order
	}
	Include interface{}
	Items   interface{}
}

type Order struct {
	Platform    string
	Quantity    int
	Last_update string
	Platinum    int
	User        struct {
		Ingame_name      string
		Region           string
		Reputation       int
		Reputation_bonus int
		Status           string //offline,online,ingame
		Avatar           string
		Id               string
		Last_seen        string
	}
	Order_type    string
	Region        string
	Visible       bool
	Creation_date string
	Id            string
}

func find_Min_Sell_Order(orders []Order, user_status string) int {
	min := 0
	for _, o := range orders {
		if o.User.Status == user_status && o.Order_type == "sell" && (min == 0 || min > o.Platinum) {
			min = o.Platinum
		}
	}
	return min
}

func extract_WarframeMarket_Item(item string) (ret string, err error) {
	doc, err := goquery.NewDocument("https://warframe.market/items/" + item)
	if err != nil {
		return
	}

	doc.Find("script").Each(func(i int, s *goquery.Selection) {
		val, exists := s.Attr("id")
		if exists && val == "application-state" {
			ret = s.Text()
			return
		}

	})
	return
}

// Needs Tesseract as an OCR. Tesseract has to be set in the System Path Variable
func ocr(file string, output string, language string) error {
	cmd := exec.Command("tesseract", file, output, language, "hocr") //, "-psm 1" =Automatic page segmentation with OSD.
	return cmd.Run()
}

func make_screenshot(displaynum int, magnification float64, fileName string) error {
	bounds := screenshot.GetDisplayBounds(displaynum)
	min := bounds.Min
	max := bounds.Max
	bounds.Min = image.Point{int(float64(min.X) * magnification), int(float64(min.Y) * magnification)}
	bounds.Max = image.Point{int(float64(max.X) * magnification), int(float64(max.Y) * magnification)}

	img, err := screenshot.CaptureRect(bounds)
	if err != nil {
		return err
	}
	file, _ := os.Create(fileName)
	defer file.Close()
	png.Encode(file, img)
	return nil
}
